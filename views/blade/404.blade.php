@extends('layouts.app')

@section('content')
  @include('partials.page-header')

  @if (! have_posts())
    <div class="wp-block-cover"><div class="wp-block-cover__inner-container"><x-alert type="warning">
      {!! __('Sorry, but the page you are trying to view does not exist.', 'jg_theme') !!}
    </x-alert>

    {!! get_search_form(false) !!}</div></div>
  @endif
@endsection