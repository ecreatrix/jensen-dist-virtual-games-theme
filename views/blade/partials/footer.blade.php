@if (is_user_logged_in() || get_the_ID() !== (int) get_theme_mod( 'jg_signin' ))
	<footer id="mastfooter" class="content-info" style="{!! jg\Theme\Helpers::mod_image( 'jg_footer_bg', true ) !!}">
		<div class="container">
			<div class="row">
				<div class="section contact col col-md-3">
					{!! jg\Theme\Helpers::mod_image( 'jg_footer_logo' ) !!}
					{!! jg\Theme\Helpers::address( ) !!}
					{!! jg\Theme\Helpers::phone( false, 'Phone: ' ) !!}
					{!! jg\Theme\Helpers::fax( false, 'Fax: ' ) !!}
				</div>
				<div class="section social col col-md-6">
					<div class="note">#ActivatingWellness</div>
					{!! jg\Theme\Helpers::add_social_media() !!}
					<div class="row support">
						<div class="funded-by">
							<div>Funded by</div>
							{!! jg\Theme\Helpers::mod_image( 'jg_ontario_logo' ) !!}
						</div>
						<div class="sponsored-by">
							<div>Sponsored by</div>
							{!! jg\Theme\Helpers::mod_image( 'jg_sponsor_logo' ) !!}
						</div>
					</div>
				</div>
				<div class="section nav col col-md-3">
					@if (has_nav_menu('footer'))
					{!! wp_nav_menu(['theme_location' => 'footer', 'menu_class' => 'navbar-nav']) !!}
					@endif
				</div>
			</div>
		</div>
	</footer>
@endif
