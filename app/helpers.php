<?php
namespace jg\Theme;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {exit;}

if ( ! class_exists( Helpers::class ) ) {
	class Helpers {
		public function __construct() {}

		public static function add_social_media() {
			$social_info = [
				'instagram' => get_theme_mod( 'jg_instagram' ),
				'twitter'   => get_theme_mod( 'jg_twitter' ),
				'facebook'  => get_theme_mod( 'jg_facebook' )
			];

			// Stop if no values are present
			if ( ! is_array( $social_info ) || empty( $social_info ) ) {
				return '';
			}

			$output = [];
			foreach ( $social_info as $key => $link ) {
				$item = '<a class="nav-link link-' . $key . '" href="' . $link . '" target="_blank"' . '>';
				$item .= '<i class="fab fa-' . $key . '"></i>';
				$item .= '</a>';

				$output[] = '<li class="nav-item">' . $item . '</li>';
			}

			return '<ul class="social-media">' . implode( ' ', $output ) . '</ul>';
		}

		public static function address( $text = false, $prefix = false ) {
			return self::mod_contact( 'address', $text, $prefix );
		}

		// Email html 5 links
		public static function email( $text = false, $prefix = false ) {
			return self::mod_contact( 'email', $text, $prefix );
		}

		public static function fax( $text = false, $prefix = false ) {
			return self::mod_contact( 'fax', $text, $prefix );
		}

		public static function mod_contact( $key, $text, $prefix ) {
			$link = get_theme_mod( 'jg_' . $key );

			if ( ! $link ) {
				return;
			}

			if ( ! $text ) {
				$text = $link;
			}

			if ( $prefix ) {
				$text = $prefix . $text;
			}

			if ( strpos( $key, 'address' ) !== false ) {
				return '<div class="' . $key . '">' . apply_filters( 'the_content', $text ) . '</div>';
			}

			$href_prefix = '';
			if ( strpos( $key, 'phone' ) !== false || strpos( $key, 'fax' ) !== false ) {
				$href_prefix = 'tel:1+';
			} else if ( strpos( $key, 'email' ) !== false ) {
				$href_prefix = 'mailto:';
			}

			return '<a class="btn-link ' . $key . '" href="' . $href_prefix . $text . '">' . $text . '</a>';
		}

		public static function mod_image( $key, $inline = false, $atts = '' ) {
			$id = get_theme_mod( $key );
			if ( $id ) {
				if ( $inline ) {
					return 'background-image: url(\'' . $id . '\');';
				}

				// Some theme mods give the image url instead of id
				$url_to_id = attachment_url_to_postid( $id );
				if ( $url_to_id ) {
					$id = $url_to_id;
				}

				return wp_get_attachment_image( $id, 'full', false, $atts );
			}

			return false;
		}

		// Phone html 5 links
		public static function phone( $text = false, $prefix = false ) {
			return self::mod_contact( 'jg_phone', $text, $prefix );
		}

		static function theme_page( $post_id, $theme_page ) {
			if ( 'profile' === $theme_page ) {
				$theme_page = get_theme_mod( 'jg_profile' );
			} else if ( 'registration_complete' === $theme_page ) {
				$theme_page = get_theme_mod( 'jg_registration_complete' );
			} else if ( 'registration_start' === $theme_page ) {
				$theme_page = get_theme_mod( 'jg_registration_start' );
			} else if ( 'signin' === $theme_page ) {
				$theme_page = get_theme_mod( 'jg_signin' );
			}

			return [
				'page_id'      => $theme_page,
				'permalink'    => get_permalink( $theme_page ),
				'current_page' => $theme_page == $post_id
			];
		}
	}

	new Helpers();
}