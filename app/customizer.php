<?php
namespace jg\Theme;

use WP_Customize_Manager;

// Logos
add_action( 'customize_register', function ( \WP_Customize_Manager $wp_customize ) {
	$wp_customize->add_setting( 'jg_header_logo' );
	$wp_customize->add_control( new \WP_Customize_Image_Control( $wp_customize, 'jg_header_logo', [
		'label'    => 'Menu Logo',
		'section'  => 'title_tagline',
		'settings' => 'jg_header_logo',
		'priority' => 8
	] ) );

	$wp_customize->add_setting( 'jg_ontario_logo' );
	$wp_customize->add_control( new \WP_Customize_Image_Control( $wp_customize, 'jg_ontario_logo', [
		'label'    => 'Ontario Logo',
		'section'  => 'title_tagline',
		'settings' => 'jg_ontario_logo',
		'priority' => 9
	] ) );

	$wp_customize->add_setting( 'jg_sponsor_logo' );
	$wp_customize->add_control( new \WP_Customize_Image_Control( $wp_customize, 'jg_sponsor_logo', [
		'label'    => 'Sponsor Logo',
		'section'  => 'title_tagline',
		'settings' => 'jg_sponsor_logo',
		'priority' => 9
	] ) );

	$wp_customize->add_setting( 'jg_footer_logo' );
	$wp_customize->add_control( new \WP_Customize_Image_Control( $wp_customize, 'jg_footer_logo', [
		'label'    => 'Footer Logo',
		'section'  => 'title_tagline',
		'settings' => 'jg_footer_logo',
		'priority' => 10
	] ) );

	$wp_customize->add_setting( 'jg_footer_bg' );
	$wp_customize->add_control( new \WP_Customize_Image_Control( $wp_customize, 'jg_footer_bg', [
		'label'    => 'Footer Background',
		'section'  => 'title_tagline',
		'settings' => 'jg_footer_bg',
		'priority' => 25
	] ) );

	$wp_customize->add_setting( 'jg_signin_page_primary_logo' );
	$wp_customize->add_control( new \WP_Customize_Image_Control( $wp_customize, 'jg_signin_page_primary_logo', [
		'label'    => 'Sign In Page Virtual Games Logo',
		'section'  => 'title_tagline',
		'settings' => 'jg_signin_page_primary_logo',
		'priority' => 26
	] ) );
	$wp_customize->add_setting( 'jg_signin_page_secondary_logo' );
	$wp_customize->add_control( new \WP_Customize_Image_Control( $wp_customize, 'jg_signin_page_secondary_logo', [
		'label'    => 'Sign In Page Well Nation Logo',
		'section'  => 'title_tagline',
		'settings' => 'jg_signin_page_secondary_logo',
		'priority' => 26
	] ) );
	$wp_customize->add_setting( 'jg_signin_page_bg' );
	$wp_customize->add_control( new \WP_Customize_Image_Control( $wp_customize, 'jg_signin_page_bg', [
		'label'    => 'Sign In Page Background',
		'section'  => 'title_tagline',
		'settings' => 'jg_signin_page_bg',
		'priority' => 26
	] ) );
} );

// Social media and business info
add_action( 'customize_register', function ( \WP_Customize_Manager $wp_customize ) {
	$wp_customize->add_section(
		'jg_info',
		[
			'title'       => __( 'Virtual Games', 'jg_theme' ),
			'description' => __( 'Contact info and social media links', 'jg_theme' ),
			'priority'    => 150
		]
	);

	$wp_customize->add_setting(
		'jg_email',
		[
			'sanitize_callback' => 'sanitize_email' //removes all invalid characters
		]
	);
	$wp_customize->add_control(
		'jg_email',
		[
			'label'   => esc_html__( 'Email', 'jg_theme' ),
			'section' => 'jg_info',
			'type'    => 'email'
		]
	);

	$wp_customize->add_setting(
		'jg_address',
		[
			'sanitize_callback' => 'wp_filter_nohtml_kses' //removes all HTML from content
		]
	);
	$wp_customize->add_control(
		'jg_address',
		[
			'label'   => esc_html__( 'Address', 'jg_theme' ),
			'section' => 'jg_info',
			'type'    => 'textarea'
		]
	);

	$wp_customize->add_setting(
		'jg_phone',
		[
			'sanitize_callback' => 'wp_filter_nohtml_kses' //removes all HTML from content
		]
	);
	$wp_customize->add_control(
		'jg_phone',
		[
			'label'   => esc_html__( 'Phone Number', 'jg_theme' ),
			'section' => 'jg_info',
			'type'    => 'text'
		]
	);

	$wp_customize->add_setting(
		'jg_fax',
		[
			'sanitize_callback' => 'wp_filter_nohtml_kses' //removes all HTML from content
		]
	);
	$wp_customize->add_control(
		'jg_fax',
		[
			'label'   => esc_html__( 'Fax Number', 'jg_theme' ),
			'section' => 'jg_info',
			'type'    => 'text'
		]
	);

	$wp_customize->add_setting(
		'jg_instagram',
		[
			'sanitize_callback' => 'esc_url_raw' //cleans URL from all invalid characters
		]
	);
	$wp_customize->add_control(
		'jg_instagram',
		[
			'label'   => esc_html__( 'Instagram', 'jg_theme' ),
			'section' => 'jg_info',
			'type'    => 'url'
		]
	);

	$wp_customize->add_setting(
		'jg_twitter',
		[
			'sanitize_callback' => 'esc_url_raw' //cleans URL from all invalid characters
		]
	);
	$wp_customize->add_control(
		'jg_twitter',
		[
			'label'   => esc_html__( 'Twitter', 'jg_theme' ),
			'section' => 'jg_info',
			'type'    => 'url'
		]
	);

	$wp_customize->add_setting(
		'jg_facebook',
		[
			'sanitize_callback' => 'esc_url_raw' //cleans URL from all invalid characters
		]
	);
	$wp_customize->add_control(
		'jg_facebook',
		[
			'label'   => esc_html__( 'Facebook', 'jg_theme' ),
			'section' => 'jg_info',
			'type'    => 'url'
		]
	);

	$wp_customize->add_setting(
		'jg_game_start',
		[
		]
	);
	$wp_customize->add_control(
		'jg_game_start',
		[
			'label'    => esc_html__( 'Virtual Games Start Date', 'jg_theme' ),
			'section'  => 'jg_info',
			'type'     => 'date',
			'priority' => 80
		]
	);

	$wp_customize->add_setting(
		'jg_game_end',
		[
		]
	);
	$wp_customize->add_control(
		'jg_game_end',
		[
			'label'    => esc_html__( 'Virtual Games End Date', 'jg_theme' ),
			'section'  => 'jg_info',
			'type'     => 'date',
			'priority' => 80
		]
	);
} );