<?php

/**
 * Theme setup.
 */
namespace jg\Theme;
/*
 * Register the initial theme setup.
 *
 * @return void
 */
add_action( 'after_setup_theme', function () {
    add_theme_support( 'disable-custom-font-sizes' );
    add_theme_support( 'disable-custom-colors' );

    add_theme_support( 'editor-gradient-presets', [] );
    add_theme_support( 'disable-custom-gradients', true );

    /*
     * Enable theme color palette support
     * @link https://developer.wordpress.org/block-editor/developers/themes/theme-support/#block-color-palettes
     */
    add_theme_support( 'editor-color-palette', [
        [
            'name'  => __( 'Red', 'jg_theme' ),
            'slug'  => 'primary',
            'color' => '#DB0032'
        ], [
            'name'  => __( 'Dark Red', 'jg_theme' ),
            'slug'  => 'secondary',
            'color' => '#950526'
        ], [
            'name'  => __( 'Grey', 'jg_theme' ),
            'slug'  => 'tertiary',
            'color' => '#AFAFAF'
        ], [
            'name'  => __( 'Dark Grey', 'jg_theme' ),
            'slug'  => 'quaternary',
            'color' => '#F0F0F0'
        ], [
            'name'  => __( 'White', 'jg_theme' ),
            'slug'  => 'white',
            'color' => '#ffffff'
        ], [
            'name'  => __( 'Black', 'jg_theme' ),
            'slug'  => 'black',
            'color' => '#000000'
        ]
    ] );
}, 20 );