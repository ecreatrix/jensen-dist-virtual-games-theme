<?php
namespace jg\Theme;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {exit;}
if ( ! class_exists( Setup::class ) ) {
    class Setup {
        public function __construct() {
            add_action( 'after_setup_theme', [$this, 'supports'], 20 );
            add_action( 'wp_before_admin_bar_render', [$this, 'remove_toolbar_menus'] );
            //add_action( 'wp_dashboard_setup', [$this, 'remove_dashboard_widgets'] );
            add_filter( 'wp_nav_menu_objects', [$this, 'menu_account_links'], 10, 2 );
            add_filter( 'admin_footer_text', [$this, 'footer_text'] );
            add_filter( 'login_headerurl', [$this, 'login_url'] );

            add_action( 'admin_menu', [$this, 'remove_admin_menus'] );

            // For Participants
            add_action( 'admin_menu', [$this, 'hide_the_dashboard'] );
            add_filter( 'logout_redirect', [$this, 'redirect_login'], 10, 3 );
            add_filter( 'login_redirect', [$this, 'redirect_login'], 10, 3 );
            add_action( 'load-index.php', [$this, 'redirect_dashboard'] );
            add_action( 'after_setup_theme', [$this, 'remove_admin_bar'] );

        }

        public function footer_text() {
            echo 'Built for you by <a href="https://jensengroup.ca/" target="_blank">Jensen Group</a> :)';
        }

        public function hide_the_dashboard() {
            global $current_user;

            if ( is_array( $current_user->roles ) && ! in_array( 'administrator', $current_user->roles ) ) {
                remove_menu_page( 'index.php' );
            }
        }

        public function login_url() {
            return home_url();
        }

        public function menu_account_links( $items, $args ) {
            // Change 'Profile' button to Sign In if not logged in
            if ( 'top' !== $args->theme_location ) {
                return $items;
            }

            foreach ( $items as $k => $object ) {
                if ( in_array( 'profile', $object->classes ) ) {
                    if ( is_user_logged_in() ) {
                        $object->title .= ' <i class="fas fa-user"></i>';
                    } else {
                        $object->title = 'Sign In <i class="fas fa-sign-in"></i>';
                        $object->url   = get_permalink( get_theme_mod( 'jg_signin' ) );
                    }
                } else if ( in_array( 'signout', $object->classes ) ) {
                    $object->url = wp_logout_url( home_url() );
                    $object->title .= ' <i class="fas fa-sign-out"></i>';
                    if ( ! is_user_logged_in() ) {
                        $object->classes[] = 'd-none';
                    }
                }
            }

            return $items;
        }

        public function redirect_dashboard() {
            global $current_user;

            if ( is_array( $current_user->roles ) && ! in_array( 'administrator', $current_user->roles ) ) {
                wp_redirect( get_permalink( get_theme_mod( 'jg_profile' ) ) );
            }
        }

        public function redirect_login( $redirect_to, $request, $user ) {
            if ( is_a( $user, 'WP_User' ) && $user->exists() && ! in_array( 'administrator', $user->roles ) ) {
                $redirect_to = get_permalink( get_theme_mod( 'jg_profile' ) );
            }

            return $redirect_to;
        }

        public function remove_admin_bar() {
            global $current_user;

            if ( is_array( $current_user->roles ) && ! in_array( 'administrator', $current_user->roles ) ) {
                add_filter( 'show_admin_bar', '__return_false' );
            }

            return true;
        }

        public function remove_admin_menus() {
            remove_menu_page( 'edit.php' );
        }

        public function remove_toolbar_menus() {
            global $wp_admin_bar;
            $wp_admin_bar->remove_menu( 'new-post' );
        }

        /*
         * Register the initial theme setup.
         *
         * @return void
         */
        public function supports() {
            /*
             * Enable features from Soil when plugin is activated
             * @link https://roots.io/plugins/soil/
             */
            add_theme_support( 'soil', [
                'clean-up',
                'nav-walker',
                'nice-search',
                'relative-urls',
            ] );

            /*
             * Enable plugins to manage the document title
             * @link https://developer.wordpress.org/reference/functions/add_theme_support/#title-tag
             */
            add_theme_support( 'title-tag' );

            /*
             * Enable customizer logo
             * @link https://developer.wordpress.org/reference/functions/add_theme_support/#custom-logo
             */
            $defaults = [
                'height'      => 100,
                'width'       => 400,
                'flex-height' => true,
                'flex-width'  => true,
                'header-text' => ['site-title', 'site-description'],
            ];

            add_theme_support( 'custom-logo', $defaults );

            /*
             * Register navigation menus
             * @link https://developer.wordpress.org/reference/functions/register_nav_menus/
             */
            register_nav_menus( [
                'top'     => __( 'Top Nav Bar', 'jg_theme' ),
                'primary' => __( 'Primary Navigation', 'jg_theme' ),
                'footer'  => __( 'Footer Links', 'jg_theme' ),
            ] );

            /*
             * Enable post thumbnails
             * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
             */
            add_theme_support( 'post-thumbnails' );

            /*
             * Add theme support for Wide Alignment
             * @link https://wordpress.org/gutenberg/handbook/designers-developers/developers/themes/theme-support/#wide-alignment
             */
            add_theme_support( 'align-wide' );

            /*
             * Enable responsive embeds
             * @link https://wordpress.org/gutenberg/handbook/designers-developers/developers/themes/theme-support/#responsive-embedded-content
             */
            add_theme_support( 'responsive-embeds' );

            /*
             * Enable HTML5 markup support
             * @link https://developer.wordpress.org/reference/functions/add_theme_support/#html5
             */
            add_theme_support( 'html5', [
                'caption',
                'comment-form',
                'comment-list',
                'gallery',
                'search-form',
                'script',
                'style',
            ] );
        }
    }

    new Setup();
}